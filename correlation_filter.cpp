#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimized function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{ 
    int *copy = new int[height*width];
   
        int p = 0,h,w;
        //Now we are accessing it continuous manner
        for(h = 0; h < height; ++h)
        {
            for(w = 0; w < width; ++w)
            {   //if it is continuous then we can directly use a variable for indexing starting from 0
                copy[p] = 0;
                
                   for(int offy = -1; offy <= 1; ++offy)
                   {
                     if(offy + w < 0 || offy + w >= width)
                         continue;
                     for(int offx = -1; offx <= 1; ++offx)
                     {
                        if(offx + h < 0 || offx + h >= height)
                            continue;
                        copy[p] += image[(offx + h) * width + offy + w] * 
                            filter[1 + (1 + offx) * FILTER_SIZE + offy];
                      
                         
                         
                     }
                   }
                   
                   if(h > 0 && w > 0)
                    {     image[(h-1)*width + (w-1)] = 0;
                      for(int offy = -1; offy <= 1; ++offy)
                     {
                         if(offy + w-1 < 0 || offy + w-1 >= width)
                             continue;
                      for(int offx = -1; offx <= 1; ++offx)
                      {
                        if(offx + h-1 < 0 || offx + h-1 >= height)
                            continue;
                        image[(h-1)*width + (w-1)] += copy[(offx + h - 1) * width + offy + w - 1] * 
                            filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                         
                         
                     }
                   }
                  }
                  p++;
           }
                    int k = (h-1)*width + (w-1);
                    image[k] = 0;
                      for(int offy = -1; offy <= 1; ++offy)
                     {
                         if(offy + w-1 < 0 || offy + w-1 >= width)
                             continue;
                      for(int offx = -1; offx <= 1; ++offx)
                      {
                        if(offx + h-1 < 0 || offx + h-1 >= height)
                            continue;
                        image[k] += copy[(offx + h - 1) * width + offy + w - 1] * 
                            filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                         
                         
                       }
                      }
                             
                 
                
            
        }
      for(int w = 0; w< width; w++)
        {        image[(h-1)*width + w] = 0;
                      for(int offy = -1; offy <= 1; ++offy)
                     {
                         if(offy + w < 0 || offy + w >= width)
                             continue;
                      for(int offx = -1; offx <= 1; ++offx)
                      {
                        if(offx + h-1 < 0 || offx + h-1 >= height)
                            continue;
                        image[(h-1)*width + w] += copy[(offx + h-1) * width + offy + w] * 
                            filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                         
                         
                     }
                   }
             }
       
        
    
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output1.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}